import React from 'react';
export const UserCard = ({ user, changeNewHandler, changeOldHandler, changePassword, newPassword, oldPassword, deleteAccount }) => {

    return (
        <>
            <h2>My Profile</h2>
            <p>Id: {user._id}</p>
            <p>Username: {user.username}</p>
            <p>Registered: <strong>{new Date(user.createdDate).toLocaleDateString()}</strong></p>
            <p><strong> Change my password:</strong></p>
            <div className="input-field">
                <input
                    placeholder="Old password"
                    id="oldPassword"
                    type="password"
                    name="oldPassword"
                    value={oldPassword}
                    className="yellow-input"
                    onChange={changeOldHandler}
                />
            </div>

            <div className="input-field">
                <input
                    placeholder="New password"
                    id="newPassword"
                    type="password"
                    name="newPassword"
                    value={newPassword}
                    className="yellow-input"
                    onChange={changeNewHandler}
                />
            </div>
            <button className="btn grey lighten-1 black-text" style={{ marginRight: 10 }} onClick={changePassword}>Change password</button>
            <button className="btn red black-text" onClick={deleteAccount}>Delete account</button>

        </>
    )
}