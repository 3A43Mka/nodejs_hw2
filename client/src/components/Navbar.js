import React, { useContext } from 'react';
import {NavLink, useHistory} from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

export const Navbar = () => {
    const history = useHistory();
    const auth = useContext(AuthContext);
    const logoutHandler = (event) => {
        event.preventDefault();
        auth.logout();
        history.push('/');
    };

    return (<nav>
        <div className="nav-wrapper blue darken-1" style={{padding: '0 2rem'}}>
          <span className="brand-logo">Notes</span>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li><NavLink to="/notes">Notes</NavLink></li>
            <li><NavLink to="/profile">My Profile</NavLink></li>
            <li><a href="/" onClick={logoutHandler}>Logout</a></li>
          </ul>
        </div>
      </nav>);
}