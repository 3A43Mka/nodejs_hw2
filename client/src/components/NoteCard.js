import React from 'react';

export const NoteCard = ({ note, newText, changeTextHandler, editNote, checkNote, deleteNote }) => {
    return (
        <>
            <h2>Note</h2>
            <div className="input-field">
                <input
                    placeholder="Note text..."
                    id="newText"
                    type="text"
                    name="newText"
                    value={newText}
                    className="blue-input"
                    onChange={changeTextHandler}
                />
            </div>
    <p>Completed: {note.completed.toString()}</p>
            <p>Date: <strong>{new Date(note.createdDate).toLocaleDateString()}</strong></p>
            <button className="btn blue" style={{ marginRight: 10 }} onClick={editNote}>Update note</button>
            <button className="btn green" style={{ marginRight: 10 }} onClick={checkNote}>Check note</button>
            <button className="btn red" onClick={deleteNote}>Delete note</button>
        </>
    )
}