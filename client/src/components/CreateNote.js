import React from 'react';

export const CreateNote = ({ createNote, changeNewNoteHandler, newText }) => {
    return (
        <>
            <div className="input-field">
                <input
                    placeholder="New note..."
                    id="newPassword"
                    type="text"
                    name="newPassword"
                    value={newText}
                    className="blue-input"
                    onChange={changeNewNoteHandler}
                />
            </div>
            <button className="btn blue" style={{ marginBottom: 50 }} onClick={createNote}>Add note</button>

        </>
    )
}