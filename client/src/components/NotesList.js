import React from 'react';
import {Link} from 'react-router-dom';

export const NotesList = ({ notes }) => {
  if (!notes.length) {
    return <p className="center">No notes yet</p>
  }
  return (
    <table>
      <thead>
        <tr>
          <th>№</th>
          <th>Text</th>
          <th>Completed</th>
          <th>Open</th>
        </tr>
      </thead>

      <tbody>
        {notes.map((note, index) => {
          return (
            <tr key={note._id}>
              <td>{index + 1}</td>
          <td>{note.text}</td>
          <td>{note.completed.toString()}</td>
          <td><Link to={`/notes/${note._id}`} >Open</Link></td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}