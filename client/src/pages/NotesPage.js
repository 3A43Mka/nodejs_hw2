import React, { useCallback, useEffect, useState, useContext } from 'react';
import { NotesList } from '../components/NotesList';
import { Loader } from '../components/Loader';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { CreateNote } from '../components/CreateNote';
import { useMessage } from '../hooks/message.hook';

export const NotesPage = () => {
    const message = useMessage();
    const [notes, setNotes] = useState([]);
    const [newText, setNewText] = useState('');
    const { loading, request } = useHttp();
    const { token } = useContext(AuthContext);

    const fetchNotes = useCallback(async () => {
        try {
            const fetched = await request('/notes', 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setNotes(fetched.notes);
        } catch (e) {
        }
    }, [request, token]);

    const createNote = useCallback(async () => {
        try {
            const data = await request('/notes', 'POST', {text: newText}, {
                Authorization: `Bearer ${token}`
            });
            fetchNotes();
            message(data.message);
        } catch (e) {
        }
    }, [request, token, fetchNotes, newText, message]);

    const changeNewNoteHandler = event => {
        setNewText(event.target.value);
    }
    
    useEffect(() => { fetchNotes() }, [fetchNotes])

    if (loading) {
        return <Loader />;
    }

    return (
        <>
            {!loading && (
                <>
                    <CreateNote 
                    changeNewNoteHandler={changeNewNoteHandler}
                    newText={newText}
                    createNote={createNote}
                    />
                    <NotesList notes={notes} />
                </>)}
        </>
    )
}