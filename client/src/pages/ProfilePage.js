import React, { useContext, useEffect, useState, useCallback } from 'react';
import { UserCard } from '../components/UserCard';
import { Loader } from '../components/Loader';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { useMessage } from '../hooks/message.hook';
import { useHistory} from 'react-router-dom';


export const ProfilePage = () => {
    const message = useMessage();
    const { token } = useContext(AuthContext);
    const { loading, error, request, clearError } = useHttp()
    const [user, setUser] = useState(null);
    const [newPassword, setNewPassword] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    const history = useHistory();
    const auth = useContext(AuthContext);

    const changeOldHandler = event => {
        setOldPassword(event.target.value);
    }

    const changeNewHandler = event => {
        setNewPassword(event.target.value);
    }



    const getUser = useCallback(async () => {
        try {
            const fetched = await request(`/users/me`, 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setUser(fetched.user);
        } catch (e) {

        }
    }, [token, request]);

    const changePassword = useCallback(async () => {
        try {
            console.log(oldPassword);
            console.log(oldPassword);
            const data = await request('/users/me', 'PATCH', { oldPassword, newPassword }, {
                Authorization: `Bearer ${token}`
            });

            message(data.message);
        } catch (e) {

        }
    }, [token, request, message, oldPassword, newPassword]);

    const deleteAccount = useCallback(async () => {
        try {
            const data = await request('/users/me', 'DELETE', null, {
                Authorization: `Bearer ${token}`
            });
            message(data.message);        
            auth.logout();
            history.push('/');    
        } catch (e) {

        }
    }, [token, request, message, auth, history]);


    useEffect(() => {
        getUser();
    }, [getUser]);

    useEffect(() => {
        message(error);
        clearError();
    }, [error, message, clearError]);

    useEffect(() => {
        window.M.updateTextFields();
    }, []);

    if (loading) {
        return <Loader />;
    }

    return (
        <>
            {!loading && user && <UserCard user={user}
            changeOldHandler={changeOldHandler}
            changeNewHandler={changeNewHandler}
            changePassword={changePassword}
            oldPassword={oldPassword}
            newPassword={newPassword} 
            deleteAccount={deleteAccount} />}
        </>
    )
}