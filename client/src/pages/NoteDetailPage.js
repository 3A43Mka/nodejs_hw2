import React, { useContext, useEffect, useState, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { NoteCard } from '../components/NoteCard';
import { Loader } from '../components/Loader';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { useMessage } from '../hooks/message.hook';
import { useHistory} from 'react-router-dom';

export const NoteDetailPage = () => {
    const { token } = useContext(AuthContext);
    const { request, loading } = useHttp();
    const [note, setNote] = useState(null);
    const [newText, setNewText] = useState('');
    const noteId = useParams().id;
    const message = useMessage();
    const history = useHistory();

    const getNote = useCallback(async () => {
        try {

            const fetched = await request(`/notes/${noteId}`, 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setNote(fetched.note);
            setNewText(fetched.note.text);
        } catch (e) {

        }
    }, [token, noteId, request]);

    const editNote = useCallback(async () => {
        try {
            const data = await request(`/notes/${noteId}`, 'PUT', { text: newText }, {
                Authorization: `Bearer ${token}`
            });

            message(data.message);
        } catch (e) {

        }
    }, [token, request, message, noteId, newText]);

    const checkNote = useCallback(async () => {
        try {
            const data = await request(`/notes/${noteId}`, 'PATCH', null, {
                Authorization: `Bearer ${token}`
            });
            message(data.message);
            getNote();
        } catch (e) {

        }
    }, [token, request, message, noteId, getNote]);

    const deleteNote = useCallback(async () => {
        try {
            const data = await request(`/notes/${noteId}`, 'Delete', null, {
                Authorization: `Bearer ${token}`
            });
            message(data.message);
            history.push('/notes');
        } catch (e) {

        }
    }, [token, request, message, noteId, history]);

    const changeTextHandler = event => {
        setNewText(event.target.value);
    }

    useEffect(() => {
        getNote();
    }, [getNote]);

    if (loading) {
        return <Loader />;
    }

    return (
        <>
            {!loading && note && <NoteCard note={note}
            newText={newText}
            changeTextHandler={changeTextHandler}
            editNote={editNote}
            checkNote={checkNote}
            deleteNote={deleteNote}/>}
            {!note && (
                <>
                <p className="center">Note not found</p>
                </>
            )}
        </>
    )
}