const { response } = require("express");
const Note = require('../models/note');
const {Types} = require('mongoose');
const bcrypt = require('bcryptjs');

module.exports.getNotes = async (req, res) => {
    try {
        let notes = await Note.find({ userId: req.user._id });
        return res.json({ notes: notes });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get notes' });
    }
};

module.exports.createNote = async (req, res) => {
    try {
        if (!req.body.text) {
            return res.status(400).json({ message: 'No text input' });
        }
        const newNote = new Note({ userId: req.user._id, completed: false, text: req.body.text, createdDate: Date.now() });
        await newNote.save();
        return res.json({ message: 'Success' });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t add note' });
    }
};

module.exports.noteDetails = async (req, res) => {
    try {
        if (!Types.ObjectId.isValid(req.params.noteId)){
            return res.status(400).json({ message: 'invalid note id' });
        }
        let note = await Note.findOne({ _id: req.params.noteId }).exec();
        if (!note) {
            return res.status(400).json({ message: 'No note found' });
        }
        if (note.userId != req.user._id) {
            return res.status(400).json({ message: 'You have no access to this note' });
        }
        return res.json({ note: note });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get note details' });
    }
};

module.exports.editNote = async (req, res) => {
    try {
        if (!Types.ObjectId.isValid(req.params.noteId)){
            return res.status(400).json({ message: 'invalid note id' });
        }
        if (!req.body.text) {
            return res.status(400).json({ message: 'No text input' });
        }
        let note = await Note.findOne({ _id: req.params.noteId }).exec();
        if (!note) {
            return res.status(400).json({ message: 'No note found' });
        }
        if (note.userId != req.user._id) {
            return res.status(400).json({ message: 'You have no access to this note' });
        }
        await Note.findByIdAndUpdate({ _id: req.params.noteId }, { text: req.body.text });
        return res.json({ message: "Success" });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t edit note' });
    }
};

module.exports.checkNote = async (req, res) => {
    try {
        if (!Types.ObjectId.isValid(req.params.noteId)){
            return res.status(400).json({ message: 'invalid note id' });
        }
        let note = await Note.findOne({ _id: req.params.noteId }).exec();
        if (!note) {
            return res.status(400).json({ message: 'No note found' });
        }
        if (note.userId != req.user._id) {
            return res.status(400).json({ message: 'You have no access to this note' });
        }
        await Note.findByIdAndUpdate({ _id: req.params.noteId }, { completed: !note.completed });
        return res.json({ message: "Success" });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t check note' });
    }
};


module.exports.deleteNote = async (req, res) => {
    try {
        if (!Types.ObjectId.isValid(req.params.noteId)){
            return res.status(400).json({ message: 'invalid note id' });
        }
        let note = await Note.findOne({ _id: req.params.noteId }).exec();
        if (!note) {
            return res.status(400).json({ message: 'No note found' });
        }
        if (note.userId != req.user._id) {
            return res.status(400).json({ message: 'You have no access to this note' });
        }
        await Note.findByIdAndDelete({ _id: req.params.noteId });
        return res.json({ message: "Success" });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t delete note' });
    }
};