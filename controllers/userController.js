const { response } = require("express");
const User = require('../models/user');
const bcrypt = require('bcryptjs');

module.exports.getProfile = async (req, res) => {
    try {
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "Requested profile doesn't exist"});
        }
        return res.json({user: req.user});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t get profile, some error happened'});
    }
};

module.exports.deleteProfile = async (req, res) => {
    try{
        await User.findOneAndDelete({_id : req.user._id});
        return res.json({message: 'Success'});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t delete profile, some error happened'});
    }
};

module.exports.changePassword = async (req, res) => {
    try {
        if (!req.body.oldPassword || !req.body.newPassword) {
            return res.status(400).json({ message: 'Invalid credentials' });
        }
        let user = await User.findOne({_id: req.user._id});
        if (!user) {
            return res.status(400).json({message: "Requested profile doesn't exist"});
        }
        let doPasswordsMatch = await bcrypt.compare(req.body.oldPassword, user.password);
        if (!doPasswordsMatch){
            return res.status(400).json({message: "Wrong old password"});
        }
        let hash = await bcrypt.hash(req.body.newPassword, 3);
        await User.findOneAndUpdate({_id: req.user._id}, {password: hash});
        return res.json({message: 'Success'});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t update password, some error happened'});
    }
};