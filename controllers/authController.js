const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('config');
const bcrypt = require('bcryptjs');
const { response } = require('express');

const secret = config.get('secret');

module.exports.register = async (req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            return res.status(400).json({ message: 'Invalid credentials' });
        }
        const hash = await bcrypt.hash(password, 3);
        const user = new User({ username, password: hash, createdDate: Date.now() });
        await user.save();
        return res.json({ message: 'Success' });
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    
};

module.exports.login = async (req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            return res.status(400).json({ message: 'Invalid or missing credentials' })
        }
        let user = await User.findOne({ username }).exec();
        if (!user) {
            return res.status(400).json({ message: 'No user with that username and password found' });
        }
        let gotUser = { _id: user.id, username: user.username, createdDate: user.createdDate };
        let isPasswordCorrect = await bcrypt.compare(password, user.password);
        if (!isPasswordCorrect) {
            return res.status(400).json({ message: 'Wrong password' });
        }
        return res.json({ jwt_token: jwt.sign(JSON.stringify(gotUser), secret) });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};