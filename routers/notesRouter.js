const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {getNotes, createNote, noteDetails, editNote, checkNote, deleteNote} = require('../controllers/notesController');

router.get('/notes', authMiddleware, getNotes);
router.post('/notes', authMiddleware, createNote);
router.get('/notes/:noteId', authMiddleware, noteDetails);
router.put('/notes/:noteId', authMiddleware, editNote);
router.patch('/notes/:noteId', authMiddleware, checkNote);
router.delete('/notes/:noteId', authMiddleware, deleteNote);

module.exports = router;