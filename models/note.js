const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Types;
module.exports = mongoose.model('note', new Schema({
    userId: {type: Types.ObjectId, ref: 'User'},
    completed: {type: Boolean, required: true},
    text: {type: String, required: true},
    createdDate: {type: Date, required: true}
},
{ versionKey: false }));